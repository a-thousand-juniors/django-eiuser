# Django Email-identified User


## Introduction

Django Email-identified User (django-eiuser) is a reusable django app which provides a custom user model for overriding the default django user model for situations where it makes more sense to use an email address as the user identification token instead of a username.

This project was created primarily for educational purposes, to learn and teach TDD in Django. Time will tell whether it is suitable for use in production apps 😅.


## Creation Notes? Whaddat!?

This document is a documentation (LOL) of the mind of the creator while creating the project. It is written for people who, like the creator who in his junior-developer days, wonder about libraries and frameworks, and how their authors were able to conceive of and create such large bodies of code. The idea here is to demonstrate that most, if not all, of the libraries and frameworks out there started as really small, simple bodies of code, and grew into the beasts they are with time.

If I can create a library, so can you. And I hope to convince you with this documentation of my creating a library.


## How this is Structured

Each time I make a commit I will put up a _wall of text_ here to describe the motivation for the code I wrote, and the passing test for it. I'll most likely also describe the code I wrote, but that's not promised 😇. And since I can't know before hand what the SHA-5 hash of the commit will be, I'm going to leave a blank space for it, to be filled up in the next round of code writing.

You can click through the hashes to appreciate the changes for each commit as you read through this document.


## Let's Set the Ball a-Rollin'


### Bootstrapping

__commit 59386236a6a0bff3d1ab6f56e63fad6099fbf70d__

We bootstrap the project using the package [`django-classy-start`](https://pypi.org/project/django-classy-start/) which provides us a replacement command `classy-start project` which builds upon the regular command for starting projects in Django, `django-admin startproject`, to start projects with a custom user model which overrides the default auth user model.

Install with pip

```$ pip install django-classy-start```

Start project

```$ classy-start project eiuser .```

But that's not all.

The custom user model specified for us by `classy-start project` inherits from django's `AbstractUser`. But that one has a number of functionalities we do not need for our email-identified user. Besides, that one is still a username-identified user model. Skimming through the source for django's `AbstractUser`, we see that it inherits from `AbstractBaseUser` and uses the `PermissionsMixin` to _mixin_ permissions related functionalities. We can start that way, too.

We change our custom user model to inherit from `AbstractBaseUser` and _mixin_ the permissions stuff with `PermissionsMixin`.

Then we take away the `admin.py` file for now, as it registers the custom user model with a mismatching `UserAdmin` model admin.

We'll resist the attempt to make migrations until absolutely necessary, so we commit our changes here.


### Making Sure User Creation Works

While we've replaced the default auth user model with our custom user model, we can't say with certainty that it works. The basic functionality we need working here is user creation. User creation in django happens through two methods on the user manager, `create_user` and `create_superuser`. We need to assure ourselves that we can create users (of our custom user model) with appropriate states via these methods.


#### Creating Regular Users

__commit b647d1059c7ea46b0c5bbc8eedb01fd157913f57__

We write a test to assert that when `create_user()` is invoked with a name, an email and a password, an appropriate user is created.

So, we invoke the method with the correct arguments, check that the count of users has increased by exactly one, retrieve the `latest` user by creation date and assert that the initial state of this user satisfies our requirement (as tabulated below).

| Field            | Assertion for user &mdash; created with `create_user()` |
|------------------|---------------------------------------------------------|
| pk               | is the uuid                                             |
| name             | is the argument name                                    |
| email            | is the argument email                                   |
| password         | checks-out with the hash of the argument password       |
| created_at       | is set                                                  |
| activated_at     | is not set                                              |
| agreed_to_tnc_at | is not set                                              |
| is_active        | False                                                   |
| is_staff         | False                                                   |
| is_superuser     | False                                                   |


#### Creating Super Users

__commit b13d8881c89f53ed3af03a76ff21c9485dc48c6b__

We write a test to assert that when `create_super_user()` is invoked with a name, an email and a password, an appropriate user is created.

Just as we did with the regular user creation method, we invoke the method with the correct arguments, check that the count of users has increased by exactly one, retrieve the `latest` user by creation date and assert that the initial state of this user satisfies our requirement (as tabulated below).

| Field            | Assertion for user &mdash; created with `create_user()` | Assertion for superuser &mdash; created with `create_superuser()` |
|------------------|---------------------------------------------------------|-------------------------------------------------------------------|
| pk               | is the uuid                                             | is the uuid                                                       |
| name             | is the argument name                                    | is the argument name                                              |
| email            | is the argument email                                   | is the argument email                                             |
| password         | checks-out with the hash of the argument password       | checks-out with the hash of the argument password                  |
| created_at       | is set                                                  | is set                                                            |
| activated_at     | is not set                                              | is set                                                            |
| agreed_to_tnc_at | is not set                                              | is set                                                            |
| is_active        | False                                                   | True                                                              |
| is_staff         | False                                                   | True                                                              |
| is_superuser     | False                                                   | True                                                             |


### Signing Up


#### Getting the Sign Up Page

__commit 962230dfcd750c9917d68202453333c585056f63__

We write a test to assert that a `GET` request made to the url of the Sign Up page returns a response with status code 200.

In the test we also assert that the proper HTML templates are used, and that the fields `name`, `email`, `password1`, `password2`, and `agree_to_tnc` are present... and that there is a call-to-action for signing up.


#### Signing Up, For Real

__commit 7e6c87e457d8374fd9d7ea91cd92446f0eea94ec__

We write a test to assert that a `POST` request made to the url of the Sign Up page with the correct payload returns a response with an appropriate redirect.

We assert that the user count increases by one. Then we fetch the latest user (by creation time) and make assertion about their state with respect to the data in the `POST`ed payload. We, also make assertions about the their __active__ status.


##### About That Redirect

We find out that because we have to assert a redirect, we need to create the view that is redirected to. And in the spirit of TDD, we write a test for this _redirected to_ view &mdash; to assert that we can get the page and see HTML rendered from an appropriate template.


#### The Terms and Conditions Must be Agreed to!

__commit be854cb19c9b69d0024c933ebbaeb49b51c4dca1__

A `POST` request made to the url of the Sign Up page with the correct payload should _fail_ if the value of `agree_to_tnc` is `False` in the payload.

We write a test to assert that in the case of this _failure_ the signup page is displayed with an appropriate error message in its context.


#### Send Activation E-mail on Signup

__commit 604f22f4b40c4abf4ee98b0b0029198aed56273c__

We write tests to assert that an appropriate activation email is fired on successful signup.

We make things easy for us by imagining we have an email sending service. The service takes a user and sends an activation email to them. For now we don't need to worry about how the service constructs the subject, body, and other parts of the email. We just mock it out, and assert that it is called with the successfully created user.

__commit dc7ed4a4b65e30d81c71f39b42ba389f0ee9b880__

Next we write a test to check that the aforementioned service crafts an appropriate django `EmailMessage` and sends it.

We make things easy for ourselves by imagining two auxiliary services, one which crafts the subject of an activation email and another which crafts the body. With this in mind we carry out the following steps:

* mock the django `EmailMessage` class
* mock the two auxiliary services
* assert that the mocks of the auxiliary services are called appropriately
* assert that the mock of the email message class is called (to instantiate an email object) with the following arguments:
  + subject &mdash; the _return value_ of the mock of the email subject crafting service
  + body &mdash; the _return value_ of the mock of the email body crafting service
  + to &mdash; a list of the reciepients
* assert that the method `send` is invoked on the _return value_ of the mock of the email message class

By doing all of these we essentially test that an appropriate subject is created, an appropriate body is created, an appropriate django email object is instantiated with these, and the `send` method of the so-created email object is called to send the mail.

__commit d253c6edd3371f264aa40429fd65660a033fd878__

We now write a test to assert that the (account activation email) subject-creating-service crafts an appropriate subject for the activation email.

We want the subject to be in the form `Please activate your &lt;site name&gt; account!`.

Note how we make use of the Django sites framework in our solution to this problem. Also note how we override the Django setting `SITE_ID`, for our particular test, to be the id of the site object we created.

_Why did we do that?_

Well, when you set the setting `SITE_ID` in your project, Django automatically creates a site with this id for you when you make your migrations &mdash; it does this using a post-migrate signal. Since migrations occur automatically before tests are run, this Django-created site exists for all your tests.

So, the _current site_ will always be the one created by Django.

That is, unless you create a custom site yourself (after the Django default has been created) and change the `SITE_ID` setting to point to your custom site, as we have done in our test.

Please note that this is just a _hacky_ way of stepping around the default site. _Hacky_ because we end up with two sites in our database &mdash; the default site, which isn't really useful to us, and the custom site, which is the only one we'll ever use.

At a later time we'll see that instead of creating a custom site we can customize the default site using a data migration.

__commit c73f59e3d95e70353adc8a2d9904c6da8fd51b3a__

We write a test to assert that the (account activation email) body-creating-service crafts an appropriate message for the activation email. In particular, we check for an appropriate subject line and the full activation url.

We expect the relative part of the url to be made up of a url-safe token generated for the user by a token generator, and the url-safe version of the user's uuid. We expect that the full url will be composed from the current site's domain and the aforementioned relative part.

We override the `SITE_ID` setting, as we did before, in order to use a custom site as the current site. Then we assume a token generator, and mock out its token generating method.

__commit 026d7cc0f82301d90daa961a9361b96e78642c49__

Now we test the account activation token generator.

Our generator is going to extend Django's password reset token generator.

We start by testing that the (inherited) methods `.make_token()` and `.check_token()` are well-behaved on our token generator.

__commit 6c36e40b6cda8fdd52cc2c86451095f26023b88f__

That done, we move on to tackle the subtleties of our token generator. We write tests to assert that our tokens are _sensitive_ to changes in the user activation timestamp as well as the `is_active` flag.

These tests assure us that when a user activates their account, their former activation token(s) no longer check out for them.

We find that overriding `._make_hash_value()` on our token generator class to include the _trigger_ fields (`is_active` and `activation_timestamp`) in its return value does the work.


### Activating the User

__commit 3e78bcd7475461231c9ee938c8af004177344e4f__

We write a test to check that the user can activate their account by making a `GET` request to their account activation link.

We start by creating an unactivated user. Then we make a `GET` request to their activation link. Note that we don't use the naked uuid in the link, but a urlsafe version of it.

We check that:

* the status code of the response is `200`
* the response makes use of the appropriate HTML template
* the user isn't logged in (we want them to explicitly log themselves in after activation)
* the body of the response contains a call to login and a link to the login page

__commit__ 11b462fa62ca928719593d694f1f4beaa0cfc9ec

We write tests to assert that activation fails for activation links encapsulating invalidated tokens.

We create three activation links for our user. Each of the links encapsulates one of three activation tokens generated at 6-hour intervals &mdash; starting from 12 hours ago to current time. We mock out the `_now()` method of our token generator, and use its return value to simulate the change in token creation time.

_Note that this `_now()` method is the default one we inherited from Django's password activation token generator. It was specifically created for the purpose of mocking out token generation time._

With that done, we activate the account using one of the links, and then check that all of the links get invalidated on account activation. We check that `GET` requests made to the endpoints at these links respond with the appropriate (activation failure) template. We also check that the user's state is not modified by these requests.


#### Requesting a new Activation Link

Account activation links, as we have created them, can expire or become invalidated. They might even get lost in transit (LOL) and not get delivered to the user's email address.

We need to create a functionality for users to request new activation links.

A user should be able to request a new activation link when:

* account activation fails for them
* they don't receive a link which was supposed to be mailed to them

__commit ef8a3d4d72d7263ad0c5aa08a6eff6f0e63078a7__

Here we write a test to check that the response to a failed account activation request contains a call to request a new activation link.

__commit 0fb1feac4577c3e1ce2078e36169ee5b53acc1f7__

Here we write a test to check that the response to a successful signup request contains a call to request a new activation link &mdash; the newly signed up user will use this to request a new activation link if they cannot find in their mail the one that was automatically sent on signup.


#### Requesting a new Activation Link &mdash; for real this time!

__commit 4e816bb1621bcd3ed14bb4d622453481618e52ec__

We start by writing a test to check that the page for requesting a new activation link:

* makes use of the correct HTML template
* contains fields for the user to enter their email and password
* contains an appropriate call to action

__commit 0e07ed8b87e4a17629749caa93dd826636aeb572__

We then write a test to check that a user is sent an activation link when they make a request for it.

In the test we assert that when a user `POSTS` an email and password to the activation link request url:

* they are redirected to the appropriate `done` page
* the service which handles sending account activation email is fired for them &mdash; notice that we do this by mocking out this service, and asserting that it is called the right number of times with the right user

__commit 002f367554b5b065f1cc3369f7479211771ce8cd__

Next, we fortify this _fresh activation link_ requesting functionality. We don't want to give the user any clues as to whether the email they supplied exists in our database, or whether the password checks out.

We just want to tell everybody that they will receive an activation link if everything checks out. And then send them the link if the things eventually check out.

Our tests here check that _the activation email sending service_ is not fired if the `POSTED` data isn't right.

__commit fcea91439aadb4a82091c9e362da2c9487f13575__

We further fortify this to ensure links are not emailed to users who have already been activated.


### Fleshing out the Templates

__commit 7cd25a5dd111d89644269c7d3ecd948818f36796__

We write tests to assert that the templates we're fleshing out contain the appropriate links, etc.


## Let's Break Things, Shall We?

So far we've been working on the signup process. We've been writing code all along, and trusting that because the tests pass everything works. I mean, we haven't for once fired up the server and interact with our creation in the browser.

This is the time to take it for a spin.


### First, a Superuser!

__commit 9720e6fbb1c7b039da2d02727a2f1e4b54664b4c__

We first create a superuser on the command line, so that we can have access to the admin interface with that user.

Our first attempt at creating a superuser doesn't go so well. After entering all the data we're prompted for, we get an error that says our superuser creation method `create_superuser()` is missing a required argument: name.

We fix this by overriding the `REQUIRED_FIELDS` attribute on our user model.

The `REQUIRED_FIELDS` attribute of the user model lets the `Django` management command `createsuperuser` know what fields to prompt for when creating a user on the command line. The command already knows that the compulsory fieldS `USERNAME_FIELD` (`email` in our case) and `password` are required, so we don't need to specify them in `REQUIRED_FIELDS`.

After we override the `REQUIRED_FIELDS` attribute, superuser creation on the command line works. Hurrah!


### Serving the Emails

__commit 41a25f91328c8949dcd46e21dd0275286fbe8655__

Since we're in development, we don't bother ourselves setting up an SMTP server for email delivery. We simply setup a [Django file-based email backend](https://docs.djangoproject.com/en/3.1/topics/email/#file-backend) so that our emails get written to files in a directory of our choice.

Note that we add this directory to our gitignore.


### Tweaking the Templates

We style our templates with [django-crispy-forms](https://pypi.org/project/django-crispy-forms/) and [django-widget-tweaks](https://pypi.org/project/django-widget-tweaks/), using styles from the frontend framework [Bootstrap](https://getbootstrap.com).

__commits__

* __77f5769446892802876bc96fa19d1c8e9e4f2095__
* __c74f55837bc30ab02267803acebf3573b2bd951d__
* __17517b31dc3634e53aac0ab697516e031e069fe1__
* __6ad3605d193988b8da1b54512390b4e9addc8713__
* __e3f08a15f08fccb29e74ff7b81c46b0e90496e2d__
* __a4bc900e7d306606d8d93b57e15e14bbdedeafc2__
* __e3cc3526cd722844b8cd4253dae2a802629e1f45__
* __52f3ce4448de9c71845fbad2599ce0a311fb1dc9__
* __4d7433974e8ee44a43af04c4de549c640c06af32__


### Customizing the Default `Site` object

__Heads-Up:__ This section may be a little too much to digest at first. Feel free to skip it for now.

__commit 0257f5e5b098e82228926be85e8c4682c9f8e5cb__

When we enable the sites framework, Django registers a `post_migrate` signal handler which creates a default site named _example.com_ and having the domain _example.com_.

We wish to set the correct name and domain for the default site using a data migration.


#### A Migrations Test Case

We want to test that a particular migration introduces certain behavior to the data in our database.

On the whole we want to perform the following sequential steps:

* Rewind to a migration where we know for sure an old behavior was present

* Get, create or update the appropriate model objects (using the historical models available on the node at this point in the migration graph) &mdash; we might even write tests to assert the old behavior

* Fastforward to the migration which is to introduce the new behavior

* Pull the data we'd earlier created out of the database (using the historical models available on the node at this point in the migration graph) and test that they now exhibit the new behavior

We abstract this flow into a migrations test case.

Our test case takes two migration names as attributes &mdash; the last migration where the old behavior existed, and the first migration where the new behavior is to show up.


##### setUpClass()

```Python
@classmethod
def setUpClass(cls):
    cls.assert_attributes()

    super().setUpClass()
    cls.app_name = apps.get_containing_app_config(cls.__module__).name

    cls.old_behavior_migration_target = [(cls.app_name, cls.old_behavior_migration)]
    cls.new_behavior_migration_target = [(cls.app_name, cls.new_behavior_migration)]
```

Before the suite of tests in the test case is run, the test case checks for required attributes, determines the name of the app it is checking the migrations for, and then creates the appropriate migration targets/nodes.


##### setUp()

```Python
def setUp(self):
    executor = MigrationExecutor(connection)

    executor.migrate(self.old_behavior_migration_target)
    old_behavior_apps = executor.loader.project_state(self.old_behavior_migration_target).apps

    self.setUpWithOldBehavior(old_behavior_apps)

    executor.loader.build_graph()  # refresh graph for next migration
    executor.migrate(self.new_behavior_migration_target)
    self.apps = executor.loader.project_state(self.new_behavior_migration_target).apps
```

Before each test method is run, a migration executor is instantiated. Using this executor, the test database is _rewound_ to the state it should be just after the old-behavior migration is applied. The state of the app registry at that instant is captured and passed to `setUpWithOldBehavior()`, so that inside the method can have access to the historical version of the models.

That done, the test database is _fast forwarded_ to the state it should be just after the new-behavior migration is applied. The state of the app registry at this instant is saved as a property on the test case, so that the test methods can use this to get the historical version of the models.


#### Our Concrete TestCase

* We start with an empty new-behavior migration created using:

  ```
  python manage.py makemigrations <app-name> --empty
  ```

* We rename the resultant migration file appropriately

* We specify the old-behavior migration and the new-behavior migration in our test case, which inherits from the migrations test case

* In the one place we have access to the historical _old-behavior_ models, `setUpWithOldBehavior()`, we fetch the default site object from the database, and assert that it indeed has the default behavior.

* In our test methods, where we have access to the historical _new-behavior_ models, we fetch the default site object from the database, and assert that its name and domain have been customized.

---

__Fix Alert!__

__commit 12e6b5bddb21d76af01aebafade8641df69ac160__

We fix the argument list (or type signature, if you may) of the `save()` method of our `accounts.forms.UserCreationForm`.

---


### Fleshing Out the Admin Site (for the custom User model)

__commit 5e6a5523300153c1da7282993891dde05918515d__

No tests here.

We take inspiration from the source code for the default Django user-admin to register our custom user model with the admin site.


### Logging In

__commit 9a2e1f1a06b7d175606c78851f271e672eb568db__

Try logging in with an account you've created and activated on your browser. You should get an error saying the template `'registration/login.html'` cannot be found.

In this section we create this template file. For content, we start with  a slightly reworded version of our signup template.

This done, we can `GET` the login page on our browser and log in.

However, on successful login an error is thrown saying there was no match for the url `accounts/profile`.

You see, Django's built-in login view, which we are reusing here, redirects to the url `accounts/profile` on successful login. In order to complete our login functionality, we need to implement this url and its associated view &mdash; the profile view.

Our tests for this view assert that logged-in users can get their profile page while their counterparts who aren't logged in are redirected to the login page. Observe the expected redirection url in the test method and reason about it.


#### About that Redirect

__commit 1407e5c453927c872ceecc4c98db0a5d6d58430b__

We're redirecting the user to their profile page on login. Which makes sense when the user is loggin in for the first time. But becomes counterintuitive on subsequent logins.

In this section we reconfigure our project to redirect users to the path at '/home' on successful login.

We first write a test to check that users are redirected to the `home` view on successful login. Pay attention to the payload we're posting in our login test. Observe that the key for the email item is `username`, and reason about it for yourself.

We then rewrite our tests from the previous section accordingly, and massage the source code until all the tests pass.


### Logging Out

__commit 6c388ab20a98a7ea874ac54b452c267b7792ee79__

We write a test to check that getting the logout endpoint logs the user out and redirects them to the login endpoint with an appropriate message.

Note that we don't use Django's built-in logout view as is. Rather, we create our own logout view which subclasses the built-in one. We hook into it's dispatch method to add the appropriate logout message.

Also note that we create our own login view which subclasses the built-in one. We do this for consistency and explicitness. We will be subclassing any built-in auth view we need from now on.


---

__Refactoring Alert!__

__commit 13e8076f3a92164311171ae5b59bcccf54b71166__

We factor out user creation from our testcases into a mixin, `accounts.tests.mixins.UserTestCaseMixin`.

This mixin saves us keystrokes when creating users in our test data setup methods. Study how it is used and appreciate the simplicity it brings about.

---


### Changing Passwords

__commit 71feaa8fddd4dc0052c1e7f9326d42a40ffa23f4__

We flesh out the password change mechanism.

We write a test to check that a `GET` result made to the password-change endpoint returns a response with a `200 OK` status code, using the correct HTML template, and containing the correct data.

We write a second test to check that a `POST` request made to the endpoint with the correct payload effects a password change and redirects to the appropriate success url.

Note that since one of these tests updates the user's password in the database, we use the `setUp` method of the testcase to ensure that the password of the user instance outside the database is in sync with the one in the database. If we don't do this, the `self.client.force_login(self.user)` line at the beginning of tests which run after the ones which modify the password, will be attempting to authenticate a user with a stale password &mdash; which will fail.

__commit c1993fd06c57cca81d15287f06a6395073795926__

Visit the password-change url in your browser. Observe that it is _littered_ with help text for the new password field. We will clean up that clutter in this section.

We write tests to check that the aforementioned help help text is not present in the response. We also check the wording for the password confirmation field.

__commit 7a234ab8cddf81e04dcac3ac33fc2918a1fde68f__

Successfully changing passwords takes us to a password-change-done page which makes use of the admin template. We fix that now.

Our test checks that the password-change-done page renders the correct non-admin template. Observe that we check that the page contains an appropriate alert message when redirected to _from_ a password-change action.


### Resetting Passwords (Reader Exercise)

The reader should look through the source for Django's password reset views and think up how they'd build on them to bake into this project a functionality for resetting passwords.

The reader may create a branch based off of the previous commit and try out what they think up. Also, the reader should abide by the following mantra, "Test first or it's cheating 😅".

The author's solution are found under the next four headings. They are not discussed.

The reader should note that all types of active users (authenticated and otherwise) should be able to take part in the password reset flow.


#### Password Reset View

* Making a `GET` request to the endpoint

  __commit fe0e7216f94566f832fb9464e03c99c5fa644962__

* `POST`ing an email address to the endpoint

  __commit 9b8cbe173e8dc6c065e382e547ae561694d67765__


#### Password Reset Done View

__commit 8b55b549c39033a51524377fc676c58f7236f069__


#### Password Reset Confirm View

__commit 3764d37859cf93980974fce2dc323a6ed398321f__


#### Password Reset Complete View

__commit 44ff4a22116d643253b938abeb209d66cd668cf7__


---

## This is Not the End

We've created a reusable Django app for identifying authenticated users by their unique email addresses (as opposed to their usernames, as is the default in Django). We've implemented a well-tested flow for managing users. We've implemented functionalities for for the following user management tasks:

* signing up
* requesting account activation link
* activating account
* logging in
* logging out
* changing password
* requesting password reset link
* resetting password

In doing this we've improved our TDD skills, and learnt a lot about the intricacies of user management and authentication, especially as concerns Django &mdash; we've gotten super familiar with the codebase in `django.contrib.auth`.

As it stands, this project, Django EiUser, is feature complete.

Going forward, we'll be making patches to fix identified issues, and refactoring the source so that it stays as close in style to the Django source as possible.

If you identify an issue within the project (or these notes), please feel free to _file_ it as an issue on the repo. PRs/MRs are super welcome.


XO,<br>
A Thousand Juniors.
