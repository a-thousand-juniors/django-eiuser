from django.contrib.auth import get_user_model
from django.test import TestCase

UserModel = get_user_model()


class TestUserCreation(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.email = 'user@aol.com'
        cls.name = 'Nenn Mich User'
        cls.passphrase = 'Dies ist eine Passphrase'

    def test_create_user(self):
        old_user_count = UserModel.objects.count()

        UserModel.objects.create_user(name=self.name, email=self.email, password=self.passphrase)

        new_user_count = UserModel.objects.count()
        self.assertEqual(new_user_count, old_user_count + 1)
        user = UserModel.objects.latest('created_at')
        self.assertEqual(user.pk, user.uuid)
        self.assertEqual(user.name, self.name)
        self.assertEqual(user.email, self.email)
        self.assertEqual(user.check_password(self.passphrase), True)
        self.assertIsNotNone(user.created_at)
        self.assertIsNone(user.activated_at)
        self.assertIsNone(user.agreed_to_tnc_at)
        self.assertEqual(user.is_active, False)
        self.assertEqual(user.is_staff, False)
        self.assertEqual(user.is_superuser, False)

    def test_create_superuser(self):
        old_user_count = UserModel.objects.count()

        UserModel.objects.create_superuser(name=self.name, email=self.email, password=self.passphrase)

        new_user_count = UserModel.objects.count()
        self.assertEqual(new_user_count, old_user_count + 1)
        user = UserModel.objects.latest('created_at')
        self.assertEqual(user.pk, user.uuid)
        self.assertEqual(user.name, self.name)
        self.assertEqual(user.email, self.email)
        self.assertEqual(user.check_password(self.passphrase), True)
        self.assertIsNotNone(user.created_at)
        self.assertIsNotNone(user.activated_at)
        self.assertIsNotNone(user.agreed_to_tnc_at)
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_staff, True)
        self.assertEqual(user.is_superuser, True)
