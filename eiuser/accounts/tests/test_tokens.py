from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from accounts.tokens import account_activation_token_generator

UserModel = get_user_model()


class TestAccountActivationTokenGenerator(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserModel.objects.create(
            name='Nenn Mich User', email='user@aol.com', password='Dies ist eine Passphrase'
        )
        cls.token_generator = account_activation_token_generator

    def make_token(self):
        return self.token_generator.make_token(self.user)

    def check_token(self, token):
        return self.token_generator.check_token(self.user, token)

    def test_account_activation_token_generator(self):
        token = self.make_token()

        self.assertEqual(self.check_token(token), True)

    def test_token_expires_with_change_in_user_is_active_flag(self):
        token_0 = self.make_token()
        self.user.is_active = not self.user.is_active
        token_1 = self.make_token()

        # return user to state where token_0 checks out
        self.user.refresh_from_db()
        self.assertTrue(self.check_token(token_0))
        self.assertFalse(self.check_token(token_1))

    def test_token_expires_when_user_activation_timestamp_is_set(self):
        token_0 = self.make_token()
        self.user.activated_at = timezone.now()
        token_1 = self.make_token()

        # return user to state where token_0 checks out
        self.user.refresh_from_db()
        self.assertTrue(self.check_token(token_0))
        self.assertFalse(self.check_token(token_1))
