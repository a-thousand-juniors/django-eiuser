from datetime import datetime, timedelta
from unittest import mock

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from accounts.tests.mixins import UserTestCaseMixin
from accounts.tokens import account_activation_token_generator


class TestActivationConfirmView(TestCase, UserTestCaseMixin):
    token_generator = account_activation_token_generator

    @classmethod
    def setUpTestData(cls):
        super().setUpUser()
        cls.url = cls.get_activation_url(cls.user)

    def setUp(self):
        super().setUp()
        self.user.refresh_from_db()

    @classmethod
    def get_activation_url(cls, user):
        return reverse(
            'accounts:activation_confirm',
            kwargs={'uidb64': cls.get_url_safe_uuid(user), 'token': cls.make_token(user)},
        )

    def test_activate_user_account(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/activation_confirm.html')
        self.assertEqual(resp.wsgi_request.user.is_authenticated, False)
        self.assertContains(resp, 'Login')
        self.assertContains(resp, reverse('login'))
        self.user.refresh_from_db()
        self.assertEqual(self.user.is_active, True)
        self.assertIsNotNone(self.user.activated_at)

    @mock.patch('accounts.tokens.account_activation_token_generator._now')
    def test_account_activation_fails_for_link_having_invalidated_token(self, mock_now):
        now = datetime.now()
        mock_now.return_value = now - timedelta(hours=12)
        url_0 = self.get_activation_url(self.user)
        mock_now.return_value = now - timedelta(hours=6)
        url_1 = self.get_activation_url(self.user)
        mock_now.return_value = now
        url_2 = self.get_activation_url(self.user)

        # assert that the urls are all different
        urls = (url_0, url_1, url_2)
        self.assertCountEqual(list(set(urls)), urls)

        # activate account with url_1
        resp_1 = self.client.get(url_1)
        self.assertEqual(resp_1.status_code, 200)
        self.assertTemplateUsed(resp_1, 'accounts/activation_confirm.html')

        # set user activation time a minute backwards
        # this will help when we want to check that
        # with subsequent use of account activation links
        # the user's state doesn't change
        self.user.refresh_from_db()
        not_now = timezone.now() - timedelta(minutes=1)
        self.user.activated_at = not_now
        self.user.save()

        # check that url_0 is invalidated
        resp_0 = self.client.get(url_0)
        self.assertEqual(resp_0.status_code, 200)
        self.assertTemplateUsed(resp_0, 'accounts/activation_confirm_failed.html')
        self.user.refresh_from_db()
        self.assertEqual(self.user.activated_at, not_now)

        # check that url_1 is invalidated
        resp_1 = self.client.get(url_1)
        self.assertEqual(resp_1.status_code, 200)
        self.assertTemplateUsed(resp_1, 'accounts/activation_confirm_failed.html')
        self.user.refresh_from_db()
        self.assertEqual(self.user.activated_at, not_now)

        # check that url_2 is invalidated
        resp_2 = self.client.get(url_2)
        self.assertEqual(resp_2.status_code, 200)
        self.assertTemplateUsed(resp_2, 'accounts/activation_confirm_failed.html')
        self.user.refresh_from_db()
        self.assertEqual(self.user.activated_at, not_now)

    @mock.patch('accounts.tokens.account_activation_token_generator._now')
    def test_content_of_activation_failure_response(self, mock_now):
        # invalidate activation tokens for user
        self.client.get(self.url)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/activation_confirm_failed.html')
        self.assertContains(resp, 'request a new activation link')
        self.assertContains(resp, reverse('accounts:activation'))
        self.assertContains(resp, reverse('login'))
