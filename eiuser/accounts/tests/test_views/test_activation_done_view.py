from django.test import TestCase
from django.urls import reverse


class TestActivationDoneView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('accounts:activation_done')

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/activation_done.html')
        self.assertContains(resp, reverse('accounts:activation'))
