from unittest import mock

from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse

from accounts.tests.mixins import UserTestCaseMixin


class TestPasswordResetDoneView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('accounts:password_reset_done')
        cls.success_alert = 'We\'ve sent you an email containing instructions for resetting your password.'

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_reset_done.html')
        # didn't redirect from password reset request action
        # success alert shouldn't be seen
        self.assertNotContains(resp, self.success_alert)

    @mock.patch('accounts.views.PasswordResetForm.send_mail')
    def test_page_redirected_to_from_password_reset_view(self, _mock_send_mail):
        resp = self.client.post(reverse('accounts:password_reset'), {'email': self.email}, follow=True)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_reset_done.html')
        self.assertContains(resp, self.success_alert)
