from django.test import TestCase
from django.urls import reverse

from accounts.tests.mixins import UserTestCaseMixin


class TestPasswordResetConfirmView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('accounts:password_reset_complete')

        cls.new_password = 'Dies ist ein neues Passwort'
        cls.success_alert = 'You\'ve successfully reset your password. You may now go ahead and log in.'
        cls.password_reset_url = reverse(
            'accounts:password_reset_confirm', kwargs={'uidb64': cls.get_url_safe_uuid(), 'token': cls.make_token()}
        )

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_reset_complete.html')
        self.assertContains(resp, reverse('login'))
        # didn't redirect from password reset confirm action
        # success alert shouldn't be seen
        self.assertNotContains(resp, self.success_alert)

    def test_page_redirected_to_from_password_reset_confirm_view(self):
        _resp = self.client.get(self.password_reset_url, follow=True)
        payload = {'new_password1': self.new_password, 'new_password2': self.new_password}

        resp = self.client.post(_resp.wsgi_request.path, payload, follow=True)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_reset_complete.html')
        self.assertContains(resp, reverse('login'))
        # redirect from password reset confirm action
        # success alert should be seen
        self.assertContains(resp, self.success_alert)
