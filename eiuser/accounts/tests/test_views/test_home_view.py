from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from accounts.tests.mixins import UserTestCaseMixin


class TestHomeView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('home')

    def test_authenticated_user_can_get_home_page(self):
        self.client.force_login(user=self.user)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/home.html')

    def test_unauthenticated_user_cannot_get_home_page(self):
        resp = self.client.get(self.url)

        expected_redirect_url = f'{reverse("login")}?next={self.url}'
        self.assertRedirects(resp, expected_redirect_url)
