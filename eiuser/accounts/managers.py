from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, name, email, password, **extra_fields):
        email = self.normalize_email(email)
        name = self.model.normalize_username(name)
        user = self.model(name=name, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, email, password=None, **extra_fields):
        return self._create_user(name=name, email=email, password=password, **extra_fields)

    def create_superuser(self, name, email, password=None, **extra_fields):
        now = timezone.now()
        extra_fields.setdefault('activated_at', now)
        extra_fields.setdefault('agreed_to_tnc_at', now)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self._create_user(name=name, email=email, password=password, **extra_fields)
