from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LoginView as BaseLoginView,
    LogoutView as BaseLogoutView,
    PasswordChangeView as BasePasswordChangeView,
    PasswordChangeDoneView as BasePasswordChangeDoneView,
    PasswordResetCompleteView as BasePasswordResetCompleteView,
    PasswordResetConfirmView as BasePasswordResetConfirmView,
    PasswordResetView as BasePasswordResetView,
    PasswordResetDoneView as BasePasswordResetDoneView,
)
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, FormView

from . import services
from .tokens import account_activation_token_generator
from .forms import ActivationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm, UserCreationForm

UserModel = get_user_model()


class SignupView(CreateView):
    form_class = UserCreationForm
    model = UserModel
    success_url = reverse_lazy('accounts:signup_done')
    template_name = 'accounts/signup.html'

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class SignupDoneView(TemplateView):
    template_name = 'accounts/signup_done.html'


class ActivationView(FormView):
    form_class = ActivationForm
    success_url = reverse_lazy('accounts:activation_done')
    template_name = 'accounts/activation.html'

    @method_decorator(csrf_protect)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        email = UserModel.objects.normalize_email(form.cleaned_data['email'])

        try:
            user = UserModel.objects.get(email=email)
            if user.activated_at is None and user.check_password(form.cleaned_data['password']):
                services.send_activation_email(user)
        except UserModel.DoesNotExist:
            pass

        return super().form_valid(form)


class ActivationDoneView(TemplateView):
    template_name = 'accounts/activation_done.html'


class ActivationConfirmView(DetailView):
    _has_token_check_failed = False
    model = UserModel
    pk_url_kwarg = 'uidb64'

    def setup(self, request, *args, **kwargs):
        kwargs[self.pk_url_kwarg] = force_str(urlsafe_base64_decode(kwargs[self.pk_url_kwarg]))
        return super().setup(request, *args, **kwargs)

    def get_object(self, queryset=None):
        user = super().get_object(queryset)

        if not account_activation_token_generator.check_token(user, self.kwargs['token']):
            self._has_token_check_failed = True
            return user

        user.is_active = True
        user.activated_at = timezone.now()
        user.save()
        return user

    def get_template_names(self):
        if self._has_token_check_failed:
            return ['accounts/activation_confirm_failed.html']
        return ['accounts/activation_confirm.html']


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/home.html'


class LoginView(BaseLoginView):
    template_name = 'accounts/login.html'


class LogoutView(BaseLogoutView):
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        messages.info(self.request, _('You\'ve just logged out of your account.'), extra_tags='logout')

        return super().dispatch(request, *args, **kwargs)


class PasswordChangeView(BasePasswordChangeView):
    form_class = PasswordChangeForm
    success_url = reverse_lazy('accounts:password_change_done')
    template_name = 'accounts/password_change.html'

    def form_valid(self, form):
        messages.info(self.request, _('You\'ve just changed your password.'), extra_tags='password_change')
        return super().form_valid(form)


class PasswordChangeDoneView(BasePasswordChangeDoneView):
    template_name = 'accounts/password_change_done.html'


class PasswordResetView(BasePasswordResetView):
    email_template_name = 'accounts/email/password_reset_email.txt'
    form_class = PasswordResetForm
    subject_template_name = 'accounts/email/password_reset_subject.txt'
    success_url = reverse_lazy('accounts:password_reset_done')
    template_name = 'accounts/password_reset.html'

    def form_valid(self, form):
        messages.info(
            self.request,
            _('We\'ve sent you an email containing instructions for resetting your password.'),
            extra_tags='password_reset',
        )
        return super().form_valid(form)


class PasswordResetDoneView(BasePasswordResetDoneView):
    template_name = 'accounts/password_reset_done.html'


class PasswordResetConfirmView(BasePasswordResetConfirmView):
    form_class = SetPasswordForm
    success_url = reverse_lazy('accounts:password_reset_complete')
    template_name = 'accounts/password_reset_confirm.html'

    def form_valid(self, form):
        messages.info(
            self.request,
            _('You\'ve successfully reset your password. You may now go ahead and log in.'),
            extra_tags='password_reset_confirm',
        )
        return super().form_valid(form)


class PasswordResetCompleteView(BasePasswordResetCompleteView):
    template_name = 'accounts/password_reset_complete.html'
