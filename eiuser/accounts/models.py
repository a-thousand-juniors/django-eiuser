import uuid

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    uuid = models.UUIDField(_('UUID'), default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    name = models.CharField(_('name'), max_length=64)
    email = models.EmailField(_('email address'), unique=True)
    created_at = models.DateTimeField(_('creation time'), auto_now_add=True)
    activated_at = models.DateTimeField(_('activation time'), null=True, blank=True)
    agreed_to_tnc_at = models.DateTimeField(_('T&C agreement time'), null=True, blank=True)
    is_active = models.BooleanField(_('active'), default=False)
    is_staff = models.BooleanField(_('staff status'), default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        swappable = 'AUTH_USER_MODEL'
