from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from . import tokens


def send_activation_email(user):
    email = EmailMessage(subject=get_activation_email_subject(), body=get_activation_email_body(user), to=[user])
    email.send()


def get_activation_email_subject():
    current_site = Site.objects.get_current()
    return f'Please activate your {current_site.name} account!'


def get_activation_email_body(user):
    token = tokens.account_activation_token_generator.make_token(user)
    uidb64 = urlsafe_base64_encode(force_bytes(user.uuid))
    return render_to_string(
        'accounts/email/activation.txt',
        {'user': user, 'site': Site.objects.get_current(), 'token': token, 'uidb64': uidb64},
    )
