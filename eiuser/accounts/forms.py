from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import (
    PasswordChangeForm as BasePasswordChangeForm,
    PasswordResetForm as BasePasswordResetForm,
    SetPasswordForm as BaseSetPasswordForm,
    UserCreationForm as BaseUserCreationForm,
    UserChangeForm as BaseUserChangeForm,
)
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from . import services

UserModel = get_user_model()


class UserCreationForm(BaseUserCreationForm):
    agree_to_tnc = forms.BooleanField(label=_('Accept our terms of use'), required=False)

    class Meta(BaseUserCreationForm.Meta):
        model = UserModel
        fields = ['name', 'email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].help_text = None
        self.fields['password2'].label = _('Confirm password')
        self.fields['password2'].help_text = None

    def clean_agree_to_tnc(self):
        is_agreed_to = self.cleaned_data['agree_to_tnc']
        if not is_agreed_to:
            raise ValidationError(_('It is required that you agree to the terms of use.'), code='invalid')
        return is_agreed_to

    def save(self, commit=True, send_activation_email=True):
        user = super().save(commit=False)
        user.agreed_to_tnc_at = timezone.now()
        if commit:
            user.save()
        if send_activation_email:
            services.send_activation_email(user)
        return user


class AdminUserCreationForm(UserCreationForm):
    agree_to_tnc = None


class UserChangeForm(BaseUserChangeForm):
    class Meta(BaseUserChangeForm.Meta):
        model = UserModel
        fields = [
            'name',
            'email',
            'is_active',
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions',
        ]


class ActivationForm(forms.Form):
    email = forms.EmailField(
        label=_("Email"), max_length=254, widget=forms.EmailInput(attrs={'autocomplete': 'email'})
    )
    password = forms.CharField(
        label=_("Password"), strip=False, widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
    )

    def clean(self):
        validated_data = super().clean()
        try:
            user = UserModel.objects.get(email=validated_data['email'])
        except UserModel.DoesNotExist:
            raise self.get_validation_error()

        if not user.check_password(validated_data['password']):
            raise self.get_validation_error()

        return validated_data

    def get_validation_error(self):
        return ValidationError(
            _('Please enter a correct email address and password. Note that both fields may be case-sensitive.'),
            code='invalid',
        )


class PasswordChangeForm(BasePasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['new_password1'].help_text = None
        self.fields['new_password2'].label = _('Confirm new password')


class PasswordResetForm(BasePasswordResetForm):
    subject_template_name = 'accounts/email/password_reset_subject.txt'
    email_template_name = 'accounts/email/password_reset_body.txt'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].label = _('Email address')

    def save(
        self,
        domain_override=None,
        subject_template_name=None,
        email_template_name=None,
        use_https=False,
        token_generator=None,
        from_email=None,
        request=None,
        html_email_template_name=None,
        extra_email_context=None,
    ):
        return super().save(
            domain_override=domain_override,
            subject_template_name=subject_template_name or self.subject_template_name,
            email_template_name=email_template_name or self.email_template_name,
            use_https=use_https,
            token_generator=token_generator,
            from_email=from_email,
            request=request,
            html_email_template_name=html_email_template_name,
            extra_email_context=extra_email_context,
        )


class SetPasswordForm(BaseSetPasswordForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['new_password1'].help_text = None
        self.fields['new_password2'].label = _('Confirm new password')
