# Django Email-identified User


## Introduction

Django Email-identified User (`Django EiUser`) provides a custom user model for overriding the default django user model in situations where it makes more sense to use an email address as the user identification token instead of a username.

This project was created primarily for educational purposes, to learn and teach TDD in Django. If you want to learn TDD in Django by building something like this then you might like to walk through the [Django EiUser creation notes](./CREATION_NOTES.md) where the author's thought process while creating the project was captured.


## Quick Start

+ ### Clone the Project (into an explicitly named target directory)

  Think up a name for a target directory, and then clone `Django EiUser` to your local computer supplying the name you thought up as the target directory.

  For example, the command below will clone `Django EiUser` into a folder called `learnery-workspace`.

  ```
  git clone git@gitlab.com:a-thousand-juniors/django-eiuser.git learnery-workspace
  ```

  In this case `learnery` is the `Django` project we want to build. We're tacking on the __-workspace__ suffix to indicate that it is a high level directory for the project.

+ ### CD into the Cloned Project

  ```
  cd learnery-workspace
  ```

+ ### Test in a Virtual Environment

  * Create the environment

    ```
    python -m venv env
    ```

  * Activate it

    ```
    .\env\Scripts\activate
    ```

  * Install the dependencies from `requirements.txt`

    ```
    pip install -r requirements.txt
    ```

  * `cd` into the `eiuser` project folder

    ```
    cd eiuser
    ```

  * Run the tests

    ```
    python manage.py test
    ```

  The tests will fail because some important settings `Django-EiUser` reads from environment variables have not yet been set. These settings can be configured in a `.env` file.

+ ### Configure Settings in a `.env` File

  * Rename the file `.env.example` to `.env`

  * Specify a value for the `SECRET_KEY` variable in the file &mdash; you may generate a secret key on your command line this way:

    ```
    python -c "import secrets; print(secrets.token_urlsafe())"
    ```

    The command above prints a random secret to your console. Copy and use it as the value for the `SECRET_KEY` variable.

  * Specify values for the `CUSTOM_SITE_NAME` and `CUSTOM_SITE_DOMAIN` variables in the file. For example:

    ```
    CUSTOM_SITE_NAME = "Learnery"
    CUSTOM_SITE_DOMAIN = "www.learnery.com"
    ```

  * Run the tests again

    ```
    python manage.py test
    ```

    If the tests fail, then check that you have renamed your `.env.example` file to just `.env`, and that you have configured the `SECRET_KEY`, `CUSTOM_SITE_NAME` and `CUSTOM_SITE_DOMAIN` settings.

+ ### Rename Things

  As it currently stands the project is named `eiuser`.

  Your project will definitely not be called `eiuser`, so you'll have to rename the project appropriately. You can do a `Find in Folder` in your favorite text editor using `CTRL + SHIFT + F` to find every occurence of `eiuser` in the project and rename them to your project's name.

  You will have to rename the two `eiuser` folders likewise.

  Run the tests after this step to ensure that you have not broken anything.

+ ### Hack Away

  If all your tests pass at this point, then, Hurrah, you can build your project on top of this!


## Contributing

`Django EiUser` is happy to receive contributions. Please submit a PR/MR containing your contribution (including tests if it's a code contribution) and bug the maintainer to review and merge.

And don't forget to add yourself to [CONTRIBUTORS.txt](./CONTRIBUTORS.txt).
